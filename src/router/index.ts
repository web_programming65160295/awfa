import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/user',
      name: 'user',
      component: () => import('../views/UserView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/material',
      name: 'materail',
      component: () => import('../views/Material/MaterialTable.vue')
    },
    {
      path: '/stock',
      name: 'stock',
      component: () => import('../views/Material/StockItemView.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/pos-view',
      name: 'pos',
      component: () => import('../views/POS/POSView.vue')
    },
    {
      path: '/check',
      name: 'check',
      component: () => import('../views/CheckInOut.vue')
    },
    {
      path: '/invoice',
      name: 'invoice',
      component: () => import('../views/Material/InvoiceDialogView.vue')
    }, {
      path: '/checkLogin',
      name: 'checkLogin',
      component: () => import('../components/CheckInOut/CheckInOutLog.vue')
    }, {
      path: '/checkTable',
      name: 'checkTable',
      component: () => import('../components/CheckInOut/CheckInOutTable.vue')
    }, {
      path: '/addsome',
      name: 'addsome',
      component: () => import('../views/Material/AddSomeMaterialDialog.vue')
    }, {
      path: '/newulic',
      name: 'newulitycost',
      component: () => import('../views/Utilitycost Pay/UtilityNewBill.vue')
    }, {
      path: '/salary',
      name: 'salary',
      component: () => import('../views/SalaryView.vue')
    }, {
      path: '/ulic',
      name: 'utilitycost',
      component: () => import('../views/Utilitycost Pay/UtilityCost.vue')
    }, {
      path: '/img',
      name: 'image',
      component: () => import('../components/UploadUserImage.vue')
    }, {
      path: '/customer',
      name: 'customer',
      component: () => import('../views/CustomerCRUDView.vue')
    }
  ]
})

export default router
