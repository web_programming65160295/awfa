import type { UtilityDetial }  from '../utiliycost/UtilityDetail';


type UtilityCost ={
    id: number,
    creatDate: Date,
    total: number,
    totalItem: number,
    userID: number,
    utilityDetail: UtilityDetial[],
}

export type{UtilityCost}