
type StockItem = {
    id: number;
    QOH: number;
    balance: number;
    materialId: number;
};

export type { StockItem }; 