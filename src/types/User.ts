type Status = "Active" | "Inactive";
type User = {
  id: number;
  fullName: string;
  email: string;
  tel: string;
  address: string;
  rank: string;
  status: Status;
  password: string;
  user: string;
};

export type { User, Status };