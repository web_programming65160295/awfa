import type { Material } from "./Material"

type StockBillDetail = {
    id: number,
    name: string,
    amount: number,
    price: number, 
    material?:Material
    
}

export type {StockBillDetail}