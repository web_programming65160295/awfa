type Status = "Active" | "Inactive"

type Promotion = {
  id: number;
  name: string;
  shortName: string,
  startDate: Date;
  endDate: Date;
  detail: string[];
  status: Status;
  discount: number
};

export type { Promotion };