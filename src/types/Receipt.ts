import type { Customer } from "./Customer";
import type { ReceiptItem } from "./ReceiptItem";
import type { User } from "./User";

type Receipt = {
    id: number;
    createdDate: Date;
    totalBefore: number;
    discount: number;
    total: number;
    receivedAmount: number;
    change: number;
    PaymentType: string;
    userId: number;
    user?: string;
    memberId: number;
    member?: Customer;
    receiptItems?: ReceiptItem[]
}

export type { Receipt }