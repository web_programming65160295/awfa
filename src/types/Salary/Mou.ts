type mou = {
    id: number;
    name: string;
    date: Date;
    timeIn:string;
    timeOut:string;
    totalMin:number
    
}
export type { mou };