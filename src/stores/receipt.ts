import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import type { Promotion } from '@/types/Promotion'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useUserStore } from './user'
import { useCustomerStore } from './customer'
import { usePromotionStore } from './promotion'

export const useReceiptStore = defineStore('receipt', () => {
  const userStore = useUserStore()
  const memberStore = useCustomerStore()
  const promotionStore = usePromotionStore()
  const receiptDialog = ref(false)
  const receipt = ref<Receipt>({
    id: 0,
    createdDate: new Date(),
    totalBefore: 0,
    discount: 0,
    total: 0,
    receivedAmount: 0,
    change: 0,
    PaymentType: '',
    userId: userStore.currentUser!.id,
    user: userStore.currentUser!.user,
    memberId: 0,
  })
  const receiptItems = ref<ReceiptItem[]>([])
  const selectedPro = promotionStore.selectedProOb

  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.products?.id === product.id
      && item.products.subCategory === product.subCategory
      && item.products.size === product.size
      && item.products.sweetLevel === product.sweetLevel)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    }
    else {
      const newReceiptItem: ReceiptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        unit: 1,
        productsId: product.id,
        products: product
      }
      receiptItems.value.push(newReceiptItem)
      calReceipt()
    }

  }

  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit == 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }

  function calReceipt() {
    let totalDrink = 0
    let totalBakery = 0
    let totalFood = 0
    for (const item of receiptItems.value) {
      if (item.products?.category == 'drink') {
        totalDrink = totalDrink + (item.price * item.unit)
      }
      if (item.products?.category == 'bakery') {
        totalBakery = totalBakery + (item.price * item.unit)
      }
      if (item.products?.category == 'foods') {
        totalFood = totalFood + (item.price * item.unit)
      }
    }
    console.log(totalDrink)
    console.log(totalBakery)
    console.log(totalFood)
    receipt.value.totalBefore = totalBakery + totalDrink + totalFood
    let totalDiscount = 0
    let drinkDiscount = totalDrink * (promotionStore.selectedProDrink / 100)
    let bakeryDiscount = totalBakery * (promotionStore.selectedProBakery / 100)
    let foodsDiscount = totalFood * (promotionStore.selectedProFoods / 100)
    if (promotionStore.memPro === true) {
      totalDiscount = drinkDiscount + bakeryDiscount + foodsDiscount + 10
    }
    else {
      totalDiscount = drinkDiscount + bakeryDiscount + foodsDiscount
      console.log('aaafa' + drinkDiscount)
    }
    receipt.value.discount = totalDiscount
    receipt.value.total = receipt.value.totalBefore - receipt.value.discount
  }
  function showReceiptDialog() {
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true

  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      discount: 0,
      total: 0,
      receivedAmount: 0,
      change: 0,
      PaymentType: 'cash',
      userId: userStore.currentUser!.id,
      user: userStore.currentUser?.user,
      memberId: 0,
    }
    memberStore.clear()
  }

  return {
    dec, inc, removeReceiptItem, addReceiptItem, calReceipt, showReceiptDialog, clear,
    receiptDialog, receiptItems, receipt
  }
})
