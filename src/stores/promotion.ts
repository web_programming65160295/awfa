import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '@/types/Promotion'
import { useCustomerStore } from './customer'
import { useReceiptStore } from './receipt'

export const usePromotionStore = defineStore('promotion', () => {
  const promotions = ref<Promotion[]>([
    {
      id: 1,
      name: "Summer",
      shortName: 'S',
      startDate: new Date('2023-12-27'),
      endDate: new Date('2024-01-14'),
      detail: ["drink"],
      status: "Active",
      discount: 10
    },
    {
      id: 2,
      name: "Winter Combo",
      shortName: 'WC',
      startDate: new Date('2024-01-15'),
      endDate: new Date('2024-02-01'),
      detail: ["bakery", "foods"],
      status: "Active",
      discount: 5
    },
    {
      id: 3,
      name: "Year-End Feast",
      shortName: 'YEF',
      startDate: new Date('2024-12-01'),
      endDate: new Date('2024-12-31'),
      detail: ["drink", "bakery", "foods"],
      status: "Inactive",
      discount: 15
    },
    {
      id: 4,
      name: "Spring Sips",
      shortName: 'SS',
      startDate: new Date('2024-03-01'),
      endDate: new Date('2024-03-15'),
      detail: ["drink"],
      status: "Active",
      discount: 5
    },
    {
      id: 5,
      name: "Easter Delights",
      shortName: 'ED',
      startDate: new Date('2024-04-01'),
      endDate: new Date('2024-04-10'),
      detail: ["bakery"],
      status: "Active",
      discount: 10
    },
    {
      id: 6,
      name: "Summer Snacks",
      shortName: 'SSn',
      startDate: new Date('2024-05-01'),
      endDate: new Date('2024-05-15'),
      detail: ["foods"],
      status: "Active",
      discount: 5
    },
    {
      id: 7,
      name: "Back-to-School Treats",
      shortName: 'BST',
      startDate: new Date('2024-08-01'),
      endDate: new Date('2024-08-15'),
      detail: ["drink", "bakery"],
      status: "Active",
      discount: 5
    },
    {
      id: 8,
      name: "Autumn Feast",
      shortName: 'AF',
      startDate: new Date('2024-09-01'),
      endDate: new Date('2024-09-30'),
      detail: ["foods"],
      status: "Active",
      discount: 5
    },
    {
      id: 9,
      name: "Halloween Special",
      shortName: 'HS',
      startDate: new Date('2024-10-15'),
      endDate: new Date('2024-10-31'),
      detail: ["drink", "foods"],
      status: "Inactive",
      discount: 20
    },
    {
      id: 10,
      name: "Festive Bites",
      shortName: 'FB',
      startDate: new Date('2024-11-15'),
      endDate: new Date('2024-11-30'),
      detail: ["bakery"],
      status: "Active",
      discount: 5
    },
    {
      id: 11,
      name: "New Year Cheers",
      shortName: 'NYC',
      startDate: new Date('2024-12-27'),
      endDate: new Date('2025-01-14'),
      detail: ["drink"],
      status: "Active",
      discount: 5
    },
    {
      id: 12,
      name: "Winter Warmth",
      shortName: 'WW',
      startDate: new Date('2025-01-15'),
      endDate: new Date('2025-02-01'),
      detail: ["bakery", "foods"],
      status: "Active",
      discount: 5
    },
    {
      id: 13,
      name: "Valentine's Treat",
      shortName: 'VT',
      startDate: new Date('2025-02-01'),
      endDate: new Date('2025-02-14'),
      detail: ["drink"],
      status: "Inactive",
      discount: 10
    },
    {
      id: 14,
      name: "Spring Delicacies",
      shortName: 'SD',
      startDate: new Date('2025-03-01'),
      endDate: new Date('2025-03-15'),
      detail: ["foods"],
      status: "Active",
      discount: 5
    },
    {
      id: 15,
      name: "Easter Extravaganza",
      shortName: 'EE',
      startDate: new Date('2025-04-01'),
      endDate: new Date('2025-04-10'),
      detail: ["drink", "bakery"],
      status: "Active",
      discount: 5
    }
    ,
    {
      id: 16,
      name: "Member",
      shortName: 'M',
      startDate: new Date('2025-04-01'),
      endDate: new Date('2025-04-10'),
      detail: [],
      status: "Inactive",
      discount: 10
    }

  ])
  let actPro: number[] = ([])
  const selectedPro = ref<string[]>([])
  const selectedProOb = ref<Promotion[]>([])
  const selectedProDrink = ref<number>(0)
  const selectedProBakery = ref<number>(0)
  const selectedProFoods = ref<number>(0)
  const memPro = ref(false)
  const selectedProClear = ref(false)
  const customerStore = useCustomerStore()
  const receiptStore = useReceiptStore()
  const asas = ref(false)

  watch(selectedProClear, (newX) => {
    asas.value = selectedProClear.value
    clearS
  })

  watch(memPro, (newX) => {
    receiptStore.calReceipt()
  })
  watch(selectedProDrink, (newX) => {
    receiptStore.calReceipt()
  })
  watch(selectedProBakery, (newX) => {
    receiptStore.calReceipt()
  })
  watch(selectedProFoods, (newX) => {
    receiptStore.calReceipt()
  })

  function memProCheck() {
    if (customerStore.currentCustomer && customerStore.currentCustomer!.pointAmount > 10) {
      for (let i = 0; i < promotions.value.length; i++) {

        if (promotions.value[i].name === 'Member') {
          promotions.value[i].status = 'Active'
          FindActive()
          console.log('sol')
        }
      }
    } else {
      for (let i = 0; i < promotions.value.length; i++) {
        if (promotions.value[i].name === 'Member') {
          promotions.value[i].status = 'Inactive'
          FindActive()
        }
      }
    }
  }

  function FindActive() {
    actPro = []
    for (let i = 0; i < promotions.value.length; i++) {
      if (promotions.value[i].status === 'Active') {
        actPro.push(promotions.value[i].id)
      }
    }
  }

  function FindActiveName() {
    FindActive()
    const actProName = []
    let x = 0
    for (let i = 0; i < promotions.value.length; i++) {
      if (promotions.value[i].id === actPro[x]) {
        actProName.push(promotions.value[i].shortName)
        x++
      }
    }
    return actProName
  }

  function mema() {
    if (memPro.value == true) {
      return 10
    }
    else { return 0 }
  }

  function selected(selected: string[]) {
    memPro.value = false
    selectedPro.value = []
    for (let i = 0; i < selected.length; i++) {
      if (selected[i] === 'M') {
        memPro.value = true
      }
      if (selected[i] !== 'M') {
        selectedPro.value[i] = selected[i]
      }

    }

    selectedOb()
    console.log(memPro.value)
    console.log("sea " + selectedPro.value)
    console.log("d " + selectedProDrink.value)
    console.log("b " + selectedProBakery.value)
    console.log("f " + selectedProFoods.value)
  }

  function selectedOb() {
    selectedProDrink.value = 0
    selectedProBakery.value = 0
    selectedProFoods.value = 0
    let a = 0
    for (let i = 0; i < promotions.value.length; i++) {
      if (promotions.value[i].shortName === selectedPro.value[a]) {
        if (promotions.value[i].detail.includes("drink")) {
          selectedProDrink.value = selectedProDrink.value + promotions.value[i].discount
        }
        if (promotions.value[i].detail.includes("bakery")) {
          selectedProBakery.value = selectedProBakery.value + promotions.value[i].discount
        }
        if (promotions.value[i].detail.includes("foods")) {
          selectedProFoods.value = selectedProFoods.value + promotions.value[i].discount
        }
        a++
      }
      else {
        for (let i = promotions.value.length - 1; i != -1; i--) {
          if (promotions.value[i].shortName === selectedPro.value[a]) {
            if (promotions.value[i].detail.includes("drink")) {
              selectedProDrink.value = selectedProDrink.value + promotions.value[i].discount
            }
            if (promotions.value[i].detail.includes("bakery")) {
              selectedProBakery.value = selectedProBakery.value + promotions.value[i].discount
            }
            if (promotions.value[i].detail.includes("foods")) {
              selectedProFoods.value = selectedProFoods.value + promotions.value[i].discount
            }
            a++
          }
        }
      }
    }
  }

  function clear() {
    selectedPro.value = []
    selectedProClear.value = true
    console.log('clea' + selectedPro.value)
  }
  function clearS() {
    selectedProClear.value = false
    console.log('cleaCA' + selectedProClear.value)
  }
  return {
    promotions, selectedProOb, selectedProDrink, selectedProBakery, selectedProFoods, memPro, asas
    , mema, FindActive, FindActiveName, selected, memProCheck, clear
  }
})
